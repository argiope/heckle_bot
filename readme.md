# Read Me #
## Installation Instructions ##
 - first you will need to install ffmpeg and the opus libraries if you are running ubuntu you can use:
```
sudo apt-get install opus-tools
sudo apt-get install ffmpeg
```
 - Then you will need to install yarn if you have not done so. See this website for more instructions on that: [https://yarnpkg.com/lang/en/docs/install] Once that is done you can just run yarn install.
 - Then you will need a settings.json file. You can copy the settings.json.example file and remove the comments. Everything then should be installed it is just a matter of running the bot

## Starting the Bot ##
Just run it from the command line with `node index.js`

## Authenticating the Bot on Your Discord Server ##
In order to use this bot you will need to create a discord application with a bot enabled.
you can do that here: [https://discordapp.com/developers/applications/]
Once that is done you can add the bot by swapping out `client_id` with you application's client id in this link below:
`https://discordapp.com/oauth2/authorize?client_id=client_id&scope=bot`
You can get the client id from [https://discordapp.com/developers/applications/] then click on the application you wish to connect it is on the general information tab of your application. Also this bot will need to be able to see and post messages and join and speak in whatever channels it is allowed in so you will have to grant it permissions accordingly after you add it to the server.
