const Discord = require('discord.js');
const fs = require("fs");
const path = require("path");
const listenStreams = new Map();
const process = require("process");
const config = JSON.parse(fs.readFileSync("./settings.json", "utf-8"));
const ytdl = require('ytdl-core');

var lastChannel = '';
var enabled = false;

var currentConnection;
var lastConnection;

require('events').EventEmitter.defaultMaxListeners = 1500;

//const recordingsPath = fs.mkdirSync(config.recordingsPath);
const discordClient = new Discord.Client();

discordClient.login(config.discordToken).catch((e) => {
  console.log(config.discordToken);
});

discordClient.on("ready", () => {
  discordClient.fetchUser(config.users[0]).then(function(user) {
    discordClient.user.setActivity(config.activityMessage.replace('%s', user.username));
  });
});


discordClient.on("message", (message) => {
  if(message.author.bot) return;
  if(!message.guild) return;
  if(message.content.indexOf(config.prefix) !== 0) return;

  var args = message.content.split(/\s+/);
  var command = args.shift().replace(config.prefix, '');
  var member = message.member;
  var voiceChannel = member.voiceChannel;

  if (command === 'start') {
    enabled = true;
    message.reply("**Enabled:** Joining your channel. This bot will follow it's victim so if they leave the channel it will follow.");
  }

  if (command === 'join') {
    if (!member.voiceChannel) {
      message.reply(" You need to be in a voice channel first.");
      return;
    }

    enabled = true;

    message.reply(" **Enabled:** Next channel the victim joins will the bot with magically appear");
    message.channel.send('Listening in to **' + member.voiceChannel.name + '**!');

    member.voiceChannel.join().then(connection => {
      remindPerson(connection);
    }).catch(console.error);
  }

  if (command === 'stop') {
    enabled = false;
    if (voiceChannel) {
      voiceChannel.join().then((connection) => {
        voiceChannel.leave();
        message.reply(" **Disabled:** the bot is no longer stalking its victim");

      });
    }
  }

  if (command === 'status') {
    if (enabled) {
      message.reply(" **Enabled:** This bot is currently stalking its victim");
    }
    else {
      message.reply(" **Disabled:** This bot is currently not stalking its victim");
    }
  }

  if (command === 'set:target') {
    var match = message.content.match(/(<@)?(\d{18})(>)?/);
    if (match && (2 in match)) {
      config.users = [match[2]];
      discordClient.fetchUser(config.users[0]).then(function(user) {
        discordClient.user.setActivity(config.activityMessage.replace('%s', user.username));
      });
    }
  }

  if (command === 'set:sound') {
    var match = message.content.match(/http(s)?:\/\/www.youtube.com\/watch\?v=[^\s]+/)
    if (match) {
      config.video = match[0];
    }
  }

  if (command === 'set:start') {
    var match = message.content.match(/(\d+)$/)
    if (match) {
      config.startDelay = parseInt(match[1]) * 1000;
    }
  }

  if (command === 'set:stop') {
    var match = message.content.match(/(\d+)$/)
    if (match) {
      config.stopDelay = parseInt(match[1]) * 1000;
    }
  }

  if (command === 'help') {
    var help = {
      embed: {
        color: 3447003,
        title: config.botName + " Help Text",
        description: "This is a list of commands to operate " + config.botName + ".",
        fields: [
          {
            name: config.prefix + "start",
            value: "This starts " + config.botName + ". The bot will not nessarly join a channel this basically puts it into stalking mode and requires the victim to join the server, move channels, or mute and unmute themselves. It can also be triggered by server muting and unmuting"
          },
          {
            name: config.prefix + "join",
            value: "This starts the bot and joins it to your voice channel"
          },
          {
            name: config.prefix + "stop",
            value: "this stops the bot and removes it from chat"
          },
          {
            name: config.prefix + "status",
            value: "this displays whether the bot is enabled or disabled"
          },
          {
            name: config.prefix + "set:target",
            value: "This sets the target either with an at mention or by pasing the user id here"
          },
          {
            name: config.prefix + "set:sound",
            value: "This sets the sound played. Paste the full url of the youtube video after the command"
          },
          {
            name: config.prefix + "set:start",
            value: "This sets the inital delay of the sound played. Number of seconds."
          },
          {
            name: config.prefix + "set:stop",
            value: "This sets the max amount of time the sound is played. Number of seconds."
          },
          {
            name: config.prefix + "help",
            value: "this displays this help text."
          }
        ]
      }
    };
    message.channel.send(help)
      .catch(console.error);
  }

});


discordClient.on("voiceStateUpdate", (member, newMember) => {
  if (member.voiceChannel && !newMember.voiceChannel && config.users.indexOf(member.id) > -1) {
    member.voiceChannel.leave();
  }

  if (!enabled && member.voiceChannel) {
    member.voiceChannel.leave();
  }

  if (newMember.voiceChannel && (lastChannel !== newMember.voiceChannel.name || !member.voiceChannel) && config.users.indexOf(member.id) > -1 && enabled) {
    lastChannel = newMember.voiceChannel.name;
    newMember.voiceChannel.join().then(connection => {
      remindPerson(connection);
    }).catch(console.error);
  }
});

function remindPerson(connection) {
  currentConnection = connection;
  if (!currentConnection) {
    connection.on("speaking", setConnection);
  }
}

function setConnection(member, speaking) {
  if (speaking && enabled && config.users.indexOf(member.id) > -1) {
    setTimeout(() => {
      console.log('start');
      var dispatcher = currentConnection.playStream(ytdl( config.video, { filter: 'audioonly' }));
      setTimeout(function(){
        dispatcher.destroy();
        console.log('stop');
      }, config.startDelay + config.stopDelay);
    }, config.startDelay);
  }
};
